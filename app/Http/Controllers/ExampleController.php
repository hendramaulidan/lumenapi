<?php

namespace App\Http\Controllers;
use \Illuminate\Http\Request;
// use \Illuminate\Http\Response;

class ExampleController extends Controller
{
    
    public function __construct()
    {
        
        // $this->middleware('age' , ['only' => ['strRandom']]);
    }
    public function strRandom()
    {
    	return str_random(32);
    }
    public function getUser($id)
    {
    	return 'user id ='.$id;
    }
    public function profile()
    {
    	echo '<a href="' .route('profileAction').  '">profile action</a>';
    }
    public function profileAction()
    {
    	return route('profile');
    }
    public function fooBar(Request $request)
    {
    	return $request->path();
    }
    public function response()
    {
    	$data['data'] = 'data succes';
    	// return response($data , 201)->header('Content-Type','application/json');
    	return response()->json([
    		'massege' => 'fail not found',
    		'status' => false
    	],404);

    }
}
    