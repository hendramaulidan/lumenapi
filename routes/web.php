<?php

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/get', function () {
    return 'ini Get';
});

$router->post('/post', function () {
    return 'ini POST';
});


//middleware
$router->get('/admin/home',['middleware' => 'age' , function () {
    return 'umur anda cukup';
}]);
$router->get('/fail',function(){
	return 'umur anda kurang';
});
//key generate
$router->get('/key','ExampleController@strRandom');
$router->get('/user/{id}','ExampleController@getUser');


//aliases

$router->get('/profil', ['as' => 'profile','uses' => 'ExampleController@profile']);
$router->get('/profile/action',['as' =>'profileAction','uses' => 'ExampleController@profileAction']);
$router->get('/foobar', 'ExampleController@fooBar');


//response
$router->get('/response','ExampleController@response');